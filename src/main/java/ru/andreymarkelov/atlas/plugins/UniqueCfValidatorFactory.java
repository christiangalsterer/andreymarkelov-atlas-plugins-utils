package ru.andreymarkelov.atlas.plugins;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginValidatorFactory;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.ValidatorDescriptor;

public class UniqueCfValidatorFactory extends AbstractWorkflowPluginFactory implements WorkflowPluginValidatorFactory {
    public static final String JQL = "jqlstr";
    public static final String COUNT = "issuecount";
    public static final String ERROR = "error";
    public static final String ERRORVIEW = "errorview";
    public static final String ISCONFIGURED = "isConfigured";
    public static final String ISSUEPROP = "cfId";
    public static final String ISSUEPROPVIEW = "cfIdView";
    public static final String ISSUEPROPS = "issueProps";

    @Override
    public Map<String, ?> getDescriptorParams(Map<String, Object> conditionParams) {
        Map<String, Object> map = new HashMap<String, Object>();

        if (conditionParams != null && conditionParams.containsKey(JQL)) {
            map.put(JQL, extractSingleParam(conditionParams, JQL));
        } else {
            map.put(JQL, "");
        }

        if (conditionParams != null && conditionParams.containsKey(COUNT)) {
            String countStr = extractSingleParam(conditionParams, COUNT);
            map.put(COUNT, Utils.isPositiveInteger(countStr) ? Integer.valueOf(countStr) : Integer.valueOf(0));
        } else {
            map.put(COUNT, Integer.valueOf(0));
        }

        if (conditionParams != null && conditionParams.containsKey(ERROR)) {
            map.put(ERROR, extractSingleParam(conditionParams, ERROR));
        }

        if (conditionParams != null && conditionParams.containsKey(ISSUEPROP)) {
            map.put(ISSUEPROP, extractSingleParam(conditionParams, ISSUEPROP));
        } else {
            map.put(ISSUEPROP, "");
        }

        return map;
    }

    private Map<String, String> getIssueProps() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        map.put("due", "Due date");
        map.put("created", "Created");
        for (CustomField field : ComponentAccessor.getCustomFieldManager().getCustomFieldObjects()) {
            map.put(field.getId(), field.getName());
        }
        return map;
    }

    private String getParam(AbstractDescriptor descriptor, String param) {
        if (!(descriptor instanceof ValidatorDescriptor)) {
            throw new IllegalArgumentException("Descriptor must be a ValidatorDescriptor.");
        }

        ValidatorDescriptor validatorDescriptor = (ValidatorDescriptor) descriptor;
        String value = (String) validatorDescriptor.getArgs().get(param);
        if (value!=null && value.trim().length() > 0) {
            return value;
        } else {
            return "";
        }
    }

    @Override
    protected void getVelocityParamsForEdit(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
        velocityParams.put(JQL, getParam(descriptor, JQL));
        velocityParams.put(COUNT, getParam(descriptor, COUNT));
        velocityParams.put(ERROR, getParam(descriptor, ERROR));
        velocityParams.put(ISSUEPROP, getParam(descriptor, ISSUEPROP));
        velocityParams.put(ISSUEPROPS, getIssueProps());
    }

    @Override
    protected void getVelocityParamsForInput(Map<String, Object> velocityParams) {
        velocityParams.put(JQL, "");
        velocityParams.put(COUNT, Integer.valueOf(0));
        velocityParams.put(ERROR, "");
        velocityParams.put(ISSUEPROP, "");
        velocityParams.put(ISSUEPROPS, getIssueProps());
    }

    @Override
    protected void getVelocityParamsForView(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
        String jql = getParam(descriptor, JQL);
        String error = getParam(descriptor, ERROR);

        SearchService searchService = ComponentManager.getComponentInstanceOfType(SearchService.class);
        SearchService.ParseResult parseResult = searchService.parseQuery(ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser(), jql);
        if (parseResult.isValid()) {
            velocityParams.put(JQL, jql);
            velocityParams.put(ISCONFIGURED, Boolean.TRUE);
        } else {
            velocityParams.put(JQL, ComponentAccessor.getJiraAuthenticationContext().getI18nHelper().getText("ru.andreymarkelov.atlas.plugins.utils.uniquevalidator.jqloptinvalid"));
            velocityParams.put(ISCONFIGURED, Boolean.FALSE);
        }

        String issuePropView;
        String issueProp = getParam(descriptor, ISSUEPROP);
        if (issueProp.equals("due")) {
            issuePropView = "Due date";
        } else if (issueProp.equals("created")) {
            issuePropView = "Created";
        } else {
            CustomField field = ComponentAccessor.getCustomFieldManager().getCustomFieldObject(issueProp);
            if (field != null) {
                issuePropView = field.getName();
            } else {
                issuePropView = issueProp;
            }
        }

        String errorview;
        if (!Utils.isValidStr(error)) {
            error = "ru.andreymarkelov.atlas.plugins.utils.uniquevalidator.jqlunique.error";
            errorview = ComponentAccessor.getJiraAuthenticationContext().getI18nHelper().getText(error);
        } else {
            errorview = String.format(error);
        }

        velocityParams.put(ISSUEPROP, issueProp);
        velocityParams.put(ISSUEPROPVIEW, issuePropView);
        velocityParams.put(COUNT, getParam(descriptor, COUNT));
        velocityParams.put(ERROR, error);
        velocityParams.put(ERRORVIEW, errorview);
    }
}
