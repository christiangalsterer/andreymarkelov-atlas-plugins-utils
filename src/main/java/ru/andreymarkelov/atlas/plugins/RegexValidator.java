package ru.andreymarkelov.atlas.plugins;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;

public class RegexValidator implements Validator {
    private final CustomFieldManager customFieldManager;

    public RegexValidator(CustomFieldManager customFieldManager) {
        this.customFieldManager = customFieldManager;
    }

    @Override
    public void validate(Map transientVars, Map args, PropertySet ps) throws InvalidInputException, WorkflowException {
        Issue issue = (Issue) transientVars.get("issue");

        String cfId = (String) args.get(RegexCfValidatorFactory.CUSTOMFIELD);
        String regex = (String) args.get(RegexCfValidatorFactory.REGEX);
        String msg = (String) args.get(RegexCfValidatorFactory.MSG);

        if (!Utils.isValidStr(cfId) || !Utils.isValidStr(regex)) {
            return;
        }

        try {
            Long.parseLong(cfId);
        } catch (NumberFormatException nex) {
            return;
        }

        Pattern pattern;
        try {
            pattern = Pattern.compile(regex);
        } catch (Exception e) {
            return;
        }

        String errorMsg;
        if (StringUtils.isEmpty(msg)) {
            errorMsg = ComponentAccessor.getJiraAuthenticationContext().getI18nHelper().getText("ru.andreymarkelov.atlas.plugins.utils.regexvalidator.regexerror", regex);
        } else {
            errorMsg = String.format(msg, regex);
        }

        CustomField customField = customFieldManager.getCustomFieldObject(Long.parseLong(cfId));
        if (customField != null) {
            Object cfVal = issue.getCustomFieldValue(customField);
            if (cfVal == null) {
                throw new InvalidInputException(errorMsg);
            }

            Matcher m = pattern.matcher(cfVal.toString());
            if (!m.matches()) {
                if (StringUtils.isEmpty(msg)) {
                    throw new InvalidInputException(errorMsg);
                } else {
                    throw new InvalidInputException(errorMsg);
                }
            }
        }
    }
}
