package ru.andreymarkelov.atlas.plugins.amutils.conditions;

import java.util.Map;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.workflow.condition.AbstractJiraCondition;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;

public class HasAttachmentsCondition extends AbstractJiraCondition {
    @Override
    public boolean passesCondition(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
        Issue issue = (Issue) transientVars.get("issue");
        return issue.getAttachments() != null && !issue.getAttachments().isEmpty();
    }
}
