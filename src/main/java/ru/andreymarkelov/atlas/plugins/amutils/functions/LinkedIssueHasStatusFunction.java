package ru.andreymarkelov.atlas.plugins.amutils.functions;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.ofbiz.DefaultOfBizConnectionFactory;
import com.atlassian.jira.plugin.jql.function.AbstractJqlFunction;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import ru.andreymarkelov.atlas.plugins.Utils;

public class LinkedIssueHasStatusFunction extends AbstractJqlFunction {
    private static final Logger logger = Logger.getLogger(LinkedIssueHasStatusFunction.class);

    private static final String SQL = "select id from jiraissue where issuestatus in (%s) and (id in (select DESTINATION from issuelink) or id in (select SOURCE from issuelink))";

    private final IssueLinkTypeManager issueLinkTypeManager;
    private final SearchService searchService;
    private final IssueManager issueManager;
    private final ConstantsManager constantsManager;

    public LinkedIssueHasStatusFunction(
            IssueLinkTypeManager issueLinkTypeManager,
            SearchService searchService,
            IssueManager issueManager,
            ConstantsManager constantsManager) {
        this.issueLinkTypeManager = issueLinkTypeManager;
        this.searchService = searchService;
        this.issueManager = issueManager;
        this.constantsManager = constantsManager;
    }

    private List<Long> findLinkedIssues(String[] statuses, User user) throws SearchException {
        List<Long> issueKeys = new ArrayList<Long>();

        String query = String.format(SQL, getStatusIds(statuses));
        Connection conn = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        try {
            conn = new DefaultOfBizConnectionFactory().getConnection();
            pStmt = conn.prepareStatement(query);
            rs = pStmt.executeQuery();
            while (rs.next()) {
                issueKeys.add(rs.getLong(1));
            }
        } catch (DataAccessException e) {
            logger.error("Data access error occured", e);
            return null;
        } catch (SQLException e) {
            logger.error("SQL error occured", e);
            return null;
        } finally {
            Utils.closeResultSet(rs);
            Utils.closeStaement(pStmt);
            Utils.closeConnection(conn);
        }

        return issueKeys;
    }

    public JiraDataType getDataType() {
        return JiraDataTypes.ISSUE;
    }

    public List<Issue> getLinkedIssues(
            User user,
            Long issueId,
            String linkDescr) throws SearchException {
        String issueKey = issueManager.getIssueObject(issueId).getKey();
        JqlQueryBuilder queryBuilder = JqlQueryBuilder.newBuilder();
        if (linkDescr == null) {
            queryBuilder.where().issue().inFunc("linkedIssues", new String[] {issueKey});
        } else {
            queryBuilder.where().issue().inFunc("linkedIssues", new String[] {issueKey, linkDescr});
        }

        Query query = queryBuilder.buildQuery();
        List<Issue> issues = searchService.search(user, query, PagerFilter.getUnlimitedFilter()).getIssues();
        return issues;
    }

    public int getMinimumNumberOfExpectedArguments() {
        return 1;
    }

    private String getStatusIds(String[] statuses) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < statuses.length; i++) {
            if (i != 0) {
                sb.append(",");
            }
            sb.append(constantsManager.getStatusByName(statuses[i]).getId());
        }
        return sb.toString();
    }

    public List<QueryLiteral> getValues(
            QueryCreationContext queryContext,
            FunctionOperand funcOperand,
            TerminalClause termClause) {
        List<QueryLiteral> result = new LinkedList<QueryLiteral>();

        List<String> args = funcOperand.getArgs();
        int argsLen = args.size();
        User user = queryContext.getUser();

        String[] statuses = null;
        String linkType = null;

        if(argsLen == 1) {
            String status = (String) args.get(0);
            statuses = new String[] {status};
        } else if(argsLen > 1) {
            String lastArg = (String) args.get(argsLen - 1);
            if(linkExist(lastArg)) {
                linkType = lastArg;
                statuses = new String[argsLen - 1];
                for(int i = 0; i < argsLen - 1; ++i) {
                    statuses[i] = (String) args.get(i);
                }
            } else {
                statuses = new String[argsLen];
                for(int i = 0; i < argsLen; ++i) {
                    statuses[i] = (String) args.get(i);
                }
            }
        }

        Set<Issue> issues = new HashSet<Issue>();
        if (statuses != null && statuses.length > 0) {
            try {
                List<Long> issueKeys = findLinkedIssues(statuses, user);
                for (Long issueId : issueKeys) {
                    issues.addAll(getLinkedIssues(user, issueId, linkType));
                }

                Iterator<Issue> issueIter = issues.iterator();
                while (issueIter.hasNext()) {
                    Issue issue = issueIter.next();
                    result.add(new QueryLiteral(funcOperand, issue.getId()));
                }
            } catch (SearchException ex) {
                logger.error("Erro during search:" + ex.getMessage(), ex);
            }
        }

        return result;
    }

    private boolean linkExist(String linkName) {
        Collection<IssueLinkType> inwardLinks = issueLinkTypeManager.getIssueLinkTypesByInwardDescription(linkName);
        if (inwardLinks.isEmpty()) {
            Collection<IssueLinkType> outwardLinks = issueLinkTypeManager.getIssueLinkTypesByOutwardDescription(linkName);
            if (outwardLinks.isEmpty()) {
                return false;
            }
        }
        return true;
    }

    private boolean statusExist(String statusName) {
        Status status = null;
        if(StringUtils.isNotBlank(statusName)) {
            status = constantsManager.getStatusByName(statusName);
        }
        return status != null;
    }

    public MessageSet validate(
            User user,
            FunctionOperand funcOperand,
            TerminalClause termClause) {
        MessageSetImpl messageSet = new MessageSetImpl();

        List<String> args = funcOperand.getArgs();
        int argsLen = args.size();

        if (args != null && argsLen >= 1) {
            if (argsLen == 1) {
                String status = (String) args.get(0);
                if(!statusExist(status)) {
                    messageSet.addErrorMessage("Invalid Status " + status + " in " + getFunctionName());
                }
            } else if (argsLen > 1) {
                String lastArg = (String) args.get(argsLen - 1);
                if (StringUtils.isNotBlank(lastArg)) {
                    if (!linkExist(lastArg) && !statusExist(lastArg)) {
                        messageSet.addErrorMessage("Invalid Linktype or Status " + lastArg + " in " + getFunctionName());
                    }
                } else {
                    messageSet.addErrorMessage("Empty Linktype or Status in " + getFunctionName());
                }

                for (int i = 0; i < argsLen - 1; ++i) {
                    String status = (String) args.get(i);
                    if (!statusExist(status)) {
                        messageSet.addErrorMessage("Invalid Status " + status + " in " + getFunctionName());
                    }
                }
            }
        } else {
            messageSet.addErrorMessage("Minimum One argument expected for " + getFunctionName());
        }

        return messageSet;
    }
}
